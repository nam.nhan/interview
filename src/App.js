import { useState, createContext, useMemo, useContext } from "react";
import "./App.css";
import DataList from "./components/DataList";
import Toggle from "./components/Toggle";

const data = [
  { name: "Daniel", age: 25 },
  { name: "John", age: 24 },
  { name: "Jen", age: 31 },
];

const languages = ["Javascript", "Python"];

const Context = createContext(null);

const Provider = ({ children }) => {
  const [language, setLanguage] = useState(languages[0]);
  const store = useMemo(
    () => ({
      language,
      setLanguage,
    }),
    [language]
  );

  return <Context.Provider value={{ ...store }}>{children}</Context.Provider>;
};

function MainSection() {
  const { language, setLanguage } = useContext(Context);

  const handleChangeLanguage = () => {
    if (language === languages[0]) return setLanguage(languages[1]);
    return setLanguage(languages[0]);
  };

  return (
    <div>
      <p id="favouriteLanguage">programing language: {language}</p>
      <button id="changeFavourite" onClick={handleChangeLanguage}>
        Toggle language
      </button>
    </div>
  );
}

function App() {
  //implement context here so can be used in child components
  return (
    // <div className="App">
    //   <Provider>
    //     <MainSection />
    //   </Provider>
    // </div>
    // <DataList data={data} />
    // <Toggle/>
    <>Ahihihi</>
  );
}

export default App;
