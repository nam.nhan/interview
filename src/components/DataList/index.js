import React, { useMemo } from "react";

function DataList(props) {
  const { data = [] } = props;

  const renderData = useMemo(() => {
    if (data?.length === 0) return;

    return data?.map((elm, idx) => {
      return (
        <li key={idx}>
          <span>{elm?.name}</span> <span>{elm?.age}</span>
        </li>
      );
    });
  }, [data]);

  return (
    <h2>
      Code goes here
      <ul>{renderData}</ul>
    </h2>
  );
}

export default DataList;
