import React, { useState } from "react";

function Toggle() {
  const [textButton, setTextButton] = useState(false);

  const handleClick = () => setTextButton((prevText) => !prevText);

  return <button onClick={handleClick}>{textButton ? "ON" : "OFF"}</button>;
}

export default Toggle;
